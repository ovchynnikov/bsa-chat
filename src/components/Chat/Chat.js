import React  from 'react';
import Header from '../Header/Header';
import MessageInput from '../MessageInput/MessageInput';
import MessageList from '../MessageList/MessageList';
import Preloader from '../Preloader/Preloader';
import './Chat.css';

class Chat extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			isFetching: true,
			chatHistory: null,
			currInputValue: '',
			currUser: {
				avatar: "https://resizing.flixster.com/PCEX63VBu7wVvdt9Eq-FrTI6d_4=/300x300/v1.cjs0MzYxNjtqOzE4NDk1OzEyMDA7MzQ5OzMxMQ",
				user: "Helen",
				userId: "4b003c20-1b8f-11e8-9629-c7eca82aa7bd"
		  },
			inputMode: 'create',
			editingId: null
		};

	this.handleInput = this.handleInput.bind(this);
	this.onSendMessage = this.onSendMessage.bind(this);
	this.handleDelete = this.handleDelete.bind(this);
	this.setEditMessage = this.setEditMessage.bind(this);
	}

	async componentDidMount() {
    await fetch(`${this.props.url}`)
      .then(response => response.json())
      .then(data => this.setState({
				...this.state,
				chatHistory: data,
				isFetching: false
			}));
			if(this.state.chatHistory.length > 0){
				this.countParticipants();
				this.countMessages();
				this.getLastMessageDate();
			}
	}

	countParticipants() {
		const resArr = [];
		let currHistory = [...this.state.chatHistory];

		currHistory.forEach( (item) => {
      let i = resArr.findIndex(x => x.user === item.user);
       if(i <= -1){
         resArr.push({id: item.userId, user: item.user});
       }
  })
		return resArr.length;
	}
	
	countMessages() {
		const resArr = [];
		this.state.chatHistory.forEach((item) => {
      let i = resArr.findIndex(x => x.text === item.text);
       if(i <= -1){
         resArr.push({id: item.id, text: item.text});
       }
  })

	return resArr.length;
	}

	getLastMessageDate() {
		let lastDate =  new Date(this.state.chatHistory[this.state.chatHistory.length-1].createdAt);
		let dd = lastDate.getDate();
		if (dd < 10) dd = '0' + dd;
	
		let mm = lastDate.getMonth() + 1;
		if (mm < 10) mm = '0' + mm;
	
		let yy = lastDate.getFullYear();
		if (yy < 10) yy = '0' + yy;
	
		let hrs = lastDate.getHours();
		if (hrs < 10) hrs = '0' + hrs;

		let mins = lastDate.getMinutes();
		if (mins < 10) mins = '0' + mins;

		return dd + '.' + mm + '.' + yy + ' ' + hrs + ':' + mins;
	}

	handleInput(e) {
		this.setState({
			...this.state,
			currInputValue: e.target.value
		})
	}

	onSendMessage(event) {
		event.preventDefault()
		if(this.state.inputMode === 'create' && this.state.currInputValue.trim().length > 0) {

			let newMessage = {
				avatar: this.state.currUser.avatar,
				createdAt: new Date().toISOString(),
				editedAt: "",
				id: new Date().toISOString(),
				text: this.state.currInputValue.trim(),
				user: this.state.currUser.user,
				userId: this.state.currUser.userId
			}
			this.setState(state => {
				const currHistory = [...state.chatHistory];
				const updChatHistory = currHistory.concat(newMessage);
				return {
					...state,
					chatHistory: updChatHistory,
					currInputValue: ''
				};
			});
		
		} else if(this.state.inputMode === 'update' && this.state.currInputValue.trim().length > 0) {
			this.onEditItemSubmit(event)
		} 
		
	}
	
	_handleKeyDown = (e) => {
    if (e.key === 'Enter') {
      this.onSendMessage(e);
    }
  }

	handleDelete(event) {
		this.setState(state => {
			let currHistory = [...state.chatHistory];

			const updChatHistory = currHistory.filter(message => message.id !== event.target.id)
      return {
        ...state,
        chatHistory: updChatHistory
      };
    });
	}


	onEditItemSubmit = () => {
		const messageId = this.state.editingId;
		const updatedMesseges = this.state.chatHistory.map((item) => item.id === messageId ? 
					{...item, text: this.state.currInputValue.trim(), editedAt: new Date() } : item);
    
			this.setState({
					  chatHistory: updatedMesseges,
						currInputValue: '',
						inputMode: 'create',
						editingId: null
			});
  };

	getCurrText = id => {
		const chatHistory = [...this.state.chatHistory]
		let oldMessage = chatHistory.find(item => item.id === id);
		return oldMessage.text;
	}

	setEditMessage = event => {
		const idToEdit = event.target.dataset.editid; 
		let currText = this.getCurrText(idToEdit);

	this.setState(state => {
    return {
      ...state,
			currInputValue: currText,
			inputMode: 'update',
			editingId: idToEdit
    }
  });
	}
	
	render() {
		if(this.state.isFetching) {
			return <Preloader />
		}
			  return 	(<div className="chat"> 
							<Header 
							chatParticipants={this.countParticipants()} 
							countedMessages={this.countMessages()} 
							lastMessageDate={this.getLastMessageDate()}
							/>
              <MessageList 
							messages={this.state.chatHistory} 
							currUserName={this.state.currUser.user} 
							handleDelete={this.handleDelete} 
							setEditMessage={this.setEditMessage}
							/>
							<MessageInput onChange={this.handleInput} 
							onSubmitHandler={this.onSendMessage} 
							value={this.state.currInputValue} 
							mode={this.state.inputMode}
							onKeyDown={this._handleKeyDown}
							/>
						</div>)
	}
}

export default Chat;