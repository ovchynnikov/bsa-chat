import React from 'react';
import './Header.css';

function Header(props) {
        
            return (<>
                <header className="header">
                    <div>
									<h1 className="header-title">Chat Name</h1>
                     <p className="header-users-count">{props.chatParticipants}</p>
                     <p className="header-messages-count">{props.countedMessages}</p>
                     <p className="header-last-message-date">{props.lastMessageDate}</p>
                    </div>
                </header>
                </>
            )
}

export default Header;