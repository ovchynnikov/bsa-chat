import React from 'react';
import Message from '../Message/Message';
import OwnMessage from '../OwnMessage/OwnMessage';
import './MessageList.css';

const MessageList = props => {
	const userName = props.currUserName;

	function getMessageTime(createdAt) {
		let messageTime = new Date(createdAt);
		let hrs = messageTime.getHours();
		let mins = messageTime.getMinutes();
		if (mins < 10) mins = '0' + mins;
		if (hrs < 10) hrs = '0' + hrs;

		return hrs + ':' + mins
	}
	
	let prevDate = null;
	let printDivider = false;
	let dateToShow = null;
	const dateToShowToday = 'Today';
	const dateToShowYesterday = 'Yesterday';
	const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
	const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"];

    return (
        <div className="message-list">
					{props.messages.map( (message) => {
						
					if (prevDate !== message.createdAt.slice(0,10)) {
						let today = new Date();
						let todayShort = new Date().toISOString().slice(0,10);

						let yesterday = new Date(today);
						yesterday.setDate(yesterday.getDate() - 1);
						let yesterdayShort = yesterday.toISOString().slice(0,10);
						let msgDate = new Date(message.createdAt);
						let dayName = days[msgDate.getDay()];
						let monthName = monthNames[msgDate.getMonth()];
						let formatedDate = `${dayName}, ${msgDate.getDate()} ${monthName}`;
						  prevDate = message.createdAt.slice(0,10);
						  dateToShow = yesterdayShort === message.createdAt.slice(0,10) ? dateToShowYesterday : (todayShort === message.createdAt.slice(0,10) ? dateToShowToday : formatedDate);
							printDivider = true;
					} else printDivider = false;

					if(message.user === userName) {
						return <React.Fragment key={`fragment_${message.id}`}>
							{printDivider ? <div key={prevDate} className="messages-divider">{dateToShow}<hr /></div> : null}
							<OwnMessage key={message.id} message={message} time={getMessageTime(message.createdAt)} handleDelete={props.handleDelete} setEditMessage={props.setEditMessage}/>
							</React.Fragment>
					} else {
							return <React.Fragment key={`fragment_${message.id}`}>
							{printDivider ? <div key={prevDate} className="messages-divider">{dateToShow}<hr /></div> : null}
							<Message key={message.id} message={message} time={getMessageTime(message.createdAt)}/>
							</React.Fragment>
						}
				}
					)}
        </div>
    )
}

export default MessageList;