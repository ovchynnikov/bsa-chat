import React from 'react';
import './OwnMessage.css';

const OwnMessage = props => {
    return (
			<div className="own-message">
				<p className="own-message-text">{props.message.text}</p>
				
				<div className="own-button-wrapper">
				<span className="own-time-span message-time">{props.time}</span>
						  <button className="message-edit" onClick={props.setEditMessage} data-editid={props.message.id}>Edit</button>
						  <button className="message-delete" onClick={props.handleDelete} id={props.message.id}>Delete</button>
						</div>
	    </div>
    )
}

export default OwnMessage;