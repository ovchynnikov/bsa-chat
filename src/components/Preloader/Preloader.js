import React from 'react';
import './Preloader.css';

const Preloader = () => (
		<div className="loader-wrapper preloader">
    	<div className="lds-dual-ring"></div>
    </div>);

export default Preloader;